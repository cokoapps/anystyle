const components = require('./components')

module.exports = {
  components,
  db: {},
  // pool: { min: 0, max: 10, idleTimeoutMillis: 1000 },
  port: 3000,
  useGraphQLServer: false,

  onStartup: [
    {
      label: 'Create client runner through environment',
      execute: async () => {
        const {
          createServiceClientFromEnvironment,
          /* eslint-disable-next-line global-require */
        } = require('@coko/service-auth')

        await createServiceClientFromEnvironment()
      },
    },
  ],
}
