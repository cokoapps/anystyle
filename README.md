# Anystyle service

A version of this is on DockerHub: https://hub.docker.com/r/cokoapps/anystyle

## The service

This standalone service exposes the functionality of converting a text string of references to XML or CSL, using Anystyle (https://anystyle.io). The API offered is as follows:

- `GET /healthcheck`
- `POST /api/auth`
- `POST /info`
- `POST /api/referencesToXml`

1. As this service is meant to be from many clients, the first endpoint allows clients to acquire their access tokens in order to be able to use the service\*. A call to the `api/auth` endpoint considered valid if it contains the Authorization header with value `Basic base64encoded(clientId:clientSecret)`.  
   The response of a valid call to the `api/auth` will return an `accessToken`\*\* (JWT)
2. A call to the `api/referencesToXml` in order to be considered valid should have the `Authorization` property in its headers with value `Bearer <the value of an accessToken provided by this service>`. The property `Content-Type` should be `multipart/form-data` and finally the body should contain a text (.txt) file under a form field called `txt`.
   The response of this endpoint will be a file stream (mime type text/xml or application/JSON) containing the XML or CSL-JSON file which was created or a `JSON` if any errors occurred.

\*client's registration required beforehand  
\*\*the life span of an accessToken is 8 hours

## Starting the service

The service is fully dockerized for ease of use for either development or for use in production.

### Development

In the root of the service run `docker-compose up`  
This container includes the actual service as well as a Postgres DB

### Production

In the root of the service run `docker-compose -f docker-compose.production.yml up`
This container contains just the service. An external Postgres DB should be provided.

### Required env variables

```
PUBSWEET_SECRET
SERVER_PORT
SERVER_HOST
SERVER_PROTOCOL
POSTGRES_USER
POSTGRES_PASSWORD
POSTGRES_HOST
POSTGRES_DB
POSTGRES_PORT
NODE_ENV=production
```

All the above are required

## Creating client's credentials

When the service is up by executing `docker exec -it <name_of_the_pagedjs_server_container> yarn create:client`.  
The above will produce a valid pair of clientId and clientSecret

## History

This was forked from the PagedJS service, https://gitlab.coko.foundation/cokoapps/pagedjs

Version numbers should be the same as what's on DockerHub.

- version 1.1.0 supports the `/api/referencesToCsl` endpoint
- version 1.1.1 is built for both AMD and Arm

## Testing notes

- to check if it's running, `GET` `localhost:3000/healthcheck`
- to get an access token, `POST` `localhost:3000/api/auth` with Basic Auth and `clientID`/`clientSecret` from `createServiceClient.js`
- once you have an auth token, `POST` `localhost:3000/info` with the access token as a Bearer Token will get info
- To get XML out of this , `POST` `localhost:3000/api/referencesToXml` with the access token as a Bearer Token, and with `txt` in the body as an attached plain text file
- To get CSL out of this, `POST` `localhost:3000/api/referencesToCsl` with the access token as a Bearer Token, and with `txt` in the body as an attached plain text file
