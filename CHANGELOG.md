## [2.0.4](https://gitlab.coko.foundation/cokoapps/anystyle/compare/v2.0.3...v2.0.4) (2025-02-04)


### Bug Fixes

* fix postgres ssl connections ([776f1df](https://gitlab.coko.foundation/cokoapps/anystyle/commit/776f1df5d71f6abb14d27495fcafe8aa889919a2))

## [2.0.3](https://gitlab.coko.foundation/cokoapps/anystyle/compare/v2.0.2...v2.0.3) (2024-12-05)

## [2.0.2](https://gitlab.coko.foundation/cokoapps/anystyle/compare/v2.0.1...v2.0.2) (2024-12-04)


### Bug Fixes

* fix hanging create client script and switch to debian ([c070826](https://gitlab.coko.foundation/cokoapps/anystyle/commit/c07082694502bc09fa5af5d6fad984ad25b2231f))

## [2.0.1](https://gitlab.coko.foundation/cokoapps/anystyle/compare/v2.0.0...v2.0.1) (2024-10-31)


### Bug Fixes

* make sure client id and client secret exist in env config ([178912d](https://gitlab.coko.foundation/cokoapps/anystyle/commit/178912d4e209c52413160823d3d2fed08a16137f))

# [2.0.0](https://gitlab.coko.foundation/cokoapps/anystyle/compare/v1.0.0...v2.0.0) (2024-10-31)


### chore

* correct incorrect published version ([3993613](https://gitlab.coko.foundation/cokoapps/anystyle/commit/3993613e269d4e1949774c13246d5746f9c08226))


### BREAKING CHANGES

* Correct version 1.0.0 to version 2.0.0

# 1.0.0 (2024-10-31)


### Bug Fixes

* **service:** downgraded coko server because new version crashes ([8a32459](https://gitlab.coko.foundation/cokoapps/anystyle/commit/8a32459e52b794f3f2bd8a6323a4c09eac250f6f))
* **service:** turned off cron job deleting static folder ([8c62024](https://gitlab.coko.foundation/cokoapps/anystyle/commit/8c62024d51ba62f7c7848c04f6c5aeb73f6994c2))


### Build System

* rename environment variables ([c06079b](https://gitlab.coko.foundation/cokoapps/anystyle/commit/c06079b33b7b293852774726e1d6095d37542523))


### Features

* **service:** added csl export endpoint ([477654e](https://gitlab.coko.foundation/cokoapps/anystyle/commit/477654e757b56b1ae9c237bb880ce0df66c25d1b))
* **service:** coko server updated to 2.2.0 ([8d7dc7f](https://gitlab.coko.foundation/cokoapps/anystyle/commit/8d7dc7f8c216938abe77f3026132f726859ae0f6))
* **service:** updated coko server ([df34eaa](https://gitlab.coko.foundation/cokoapps/anystyle/commit/df34eaadce46f33328834fdcff2871a8adf24590))


### BREAKING CHANGES

* Renamed environment variables: `PUBLIC_URL` to `SERVER_URL`, `PUBSWEET_SECRET` to `SECRET`

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.
