FROM node:20-bookworm-slim

RUN corepack enable

ENV CONNECTION_TIMEOUT=60000

RUN apt update && apt install -y \
	build-essential \
	ruby \
	ruby-dev

# install Anystyle
RUN gem install thin
RUN gem install anystyle
RUN gem install anystyle-cli

RUN mkdir -p /home/node/Downloads

WORKDIR /home/node/anystyle

RUN chown -R node:node /home/node/anystyle

USER node

COPY --chown=node:node .yarnrc.yml .
COPY --chown=node:node package.json ./package.json
COPY --chown=node:node yarn.lock ./yarn.lock

RUN yarn workspaces focus --production && yarn cache clean && rm -rf ~/.npm

COPY --chown=node:node . .

CMD ["yarn", "coko-server", "start"]
